package com.ysfaltn.typemapper.adapter.checker;

import com.ysfaltn.typemapper.model.EnumField;
import com.ysfaltn.typemapper.model.EnumValue;
import com.ysfaltn.typemapper.model.FieldValue;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 13/11/2017.
 */
public class EnumValueCheckerTest
{
    @Test
    public void successTest()
    {
        List<EnumValue> values = new ArrayList<>();
        EnumValue active = new EnumValue();
        active.setValue("Active");
        active.setCode("active");
        values.add(active);
        EnumField enumField = new EnumField();
        enumField.setValues(values);

        FieldValue fieldValue = new FieldValue("active", "active");
        EnumValueChecker checker = new EnumValueChecker();
        Boolean result = checker.check(fieldValue, enumField);

        Assert.assertEquals(true, result);
    }

    @Test
    public void failOverTest()
    {
        List<EnumValue> values = new ArrayList<>();
        EnumValue active = new EnumValue();
        active.setValue("Active");
        active.setCode("active");
        values.add(active);
        EnumField enumField = new EnumField();
        enumField.setValues(values);

        FieldValue fieldValue = new FieldValue("active", "active1");
        EnumValueChecker checker = new EnumValueChecker();
        Boolean result = checker.check(fieldValue, enumField);

        Assert.assertEquals(false, result);
    }
}
