package com.ysfaltn.typemapper;

import com.ysfaltn.typemapper.adapter.ValueAdapter;
import com.ysfaltn.typemapper.adapter.checker.FieldValueNotCorrectlyException;
import com.ysfaltn.typemapper.exception.FieldValueCastException;
import com.ysfaltn.typemapper.model.*;
import com.ysfaltn.typemapper.repository.TypeRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.omg.CORBA.Any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by yusufaltun on 13/11/2017.
 */
public class ValueAdapterTest
{
    private ValueAdapter valueAdapter = null;

    @Before
    public void before()
    {
        TypeRepository typeRepository = Mockito.mock(TypeRepository.class);
        valueAdapter = new ValueAdapter();
        List<EnumValue> values = new ArrayList<>();
        List<Field> fields = new ArrayList<>();
        EnumField enumField = new EnumField();
        TypeModel typeModel = new TypeModel();

        valueAdapter.setTypeRepository(typeRepository);
        EnumValue active = new EnumValue();
        active.setValue("Active");
        active.setCode("active");
        values.add(active);

        enumField.setValues(values);
        enumField.setName("active");
        typeModel.setTypeName("user");
        fields.add(enumField);
        typeModel.setFields(fields);

        Mockito.when(typeRepository.getType(Mockito.anyString())).thenReturn(typeModel);
    }

    @Test
    public void adapterTest()
    {
        HashMap<String, FieldValue> fieldValues = new HashMap<>();
        TypeValue value = new TypeValue();
        value.setTypeName("user");
        FieldValue fieldValue = new FieldValue("active", "active");
        fieldValues.put("active", fieldValue);
        value.setValues(fieldValues);

        valueAdapter.check(value);
    }

    @Test(expected = FieldValueCastException.class)
    public void catchFieldValueCastException()
    {
        TypeValue value = new TypeValue();
        value.setTypeName("user");
        FieldValue fieldValue = new FieldValue("active", new Integer(2));

        value.addValue(fieldValue);
        valueAdapter.check(value);
    }

    @Test(expected = FieldValueNotCorrectlyException.class)
    public void catchFieldValueNotCorrectlyException()
    {
        TypeValue value = new TypeValue();
        value.setTypeName("user");
        FieldValue fieldValue = new FieldValue("active", "active1");

        value.addValue(fieldValue);
        valueAdapter.check(value);
    }
}
