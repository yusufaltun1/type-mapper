package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.BooleanField;
import com.ysfaltn.typemapper.model.EnumField;
import com.ysfaltn.typemapper.model.Field;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class EnumFieldResolverTest
{
    @Test
    public void shouldBeResolveJson() throws IOException
    {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        String json = "{\""+ Field.NAME +"\":\"status\", \""+ Field.TYPE +"\":\"ENUM\", \"" + Field.TEXT + "\":\"Statusler\", \"values\":[{\"code\":\"active\", \"value\":\"Aktif\"}, {\"code\":\"passive\", \"value\":\"Pasif\"}]}";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(json, JsonNode.class);

        EnumField field = (EnumField) fieldResolverMapper.getResolver(jsonNode.get("type").asText()).resolve(jsonNode);

        Assert.assertEquals("status", field.getName());
        Assert.assertEquals("ENUM", field.getType());
        Assert.assertEquals("Statusler", field.getText());

//        Assert.assertEquals("Aktif", field.getValues().get("active"));
//        Assert.assertEquals("Pasif", field.getValues().get("passive"));
    }
}
