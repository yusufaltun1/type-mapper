package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.DateField;
import com.ysfaltn.typemapper.model.Field;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class DateFieldResolverTest
{
    @Test
    public void shouldBeResolveJson() throws IOException
    {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        String json = "{\""+ Field.NAME +"\":\"birthDay\", \""+ Field.TYPE +"\":\"DATE\", \"" + Field.TEXT + "\":\"Dogum Tarihi\" }";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(json, JsonNode.class);

        DateField field = (DateField) fieldResolverMapper.getResolver(jsonNode.get("type").asText()).resolve(jsonNode);

        Assert.assertEquals("birthDay", field.getName());
        Assert.assertEquals("DATE", field.getType());
        Assert.assertEquals("Dogum Tarihi", field.getText());
    }
}
