package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.StringField;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class StringFieldResolverTest
{
    @Test
    public void shouldBeResolve() throws IOException {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        String json = "{\""+ Field.NAME +"\":\"name\", \""+ Field.TYPE +"\":\"STRING\", \"" + Field.TEXT + "\":\"AD SOYAD\"}";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(json, JsonNode.class);

        StringField field = (StringField)fieldResolverMapper.getResolver(jsonNode.get("type").asText()).resolve(jsonNode);

        Assert.assertEquals("name", field.getName());
        Assert.assertEquals("STRING", field.getType());
        Assert.assertEquals("AD SOYAD", field.getText());
    }
}
