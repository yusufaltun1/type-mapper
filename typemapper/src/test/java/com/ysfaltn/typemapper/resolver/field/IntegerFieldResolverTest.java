package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.IntegerField;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class IntegerFieldResolverTest
{
    @Test
    public void shouldBeResolveJson() throws IOException {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        String json = "{\""+ Field.NAME +"\":\"id\", \""+ Field.TYPE +"\":\"INTEGER\", \"" + Field.TEXT + "\":\"Sira Numarasi\" }";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(json, JsonNode.class);

        IntegerField field = (IntegerField)fieldResolverMapper.getResolver(jsonNode.get("type").asText()).resolve(jsonNode);

        Assert.assertEquals("id", field.getName());
        Assert.assertEquals("INTEGER", field.getType());
        Assert.assertEquals("Sira Numarasi", field.getText());
    }
}
