package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.DateField;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.TypeField;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class TypeFieldResolverTest
{
    @Test
    public void shouldBeResolveJson() throws IOException
    {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        String json = "{\""+ Field.NAME +"\":\"books\", \""+ Field.TYPE +"\":\"TYPE\", \"" + Field.TEXT + "\":\"Kitaplar\", \"ref\":{\"type\":\"book\", \"column\":\"id\"} }";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(json, JsonNode.class);

        TypeField field = (TypeField) fieldResolverMapper.getResolver(jsonNode.get("type").asText()).resolve(jsonNode);

        Assert.assertEquals("books", field.getName());
        Assert.assertEquals("TYPE", field.getType());
        Assert.assertEquals("Kitaplar", field.getText());
        Assert.assertEquals("id", field.getReference().getColumn());
        Assert.assertEquals("book", field.getReference().getType());
    }
}
