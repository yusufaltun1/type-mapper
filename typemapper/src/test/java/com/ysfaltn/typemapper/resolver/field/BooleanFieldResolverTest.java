package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.BooleanField;
import com.ysfaltn.typemapper.model.DateField;
import com.ysfaltn.typemapper.model.Field;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class BooleanFieldResolverTest
{
    @Test
    public void shouldBeResolveJson() throws IOException
    {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        String json = "{\""+ Field.NAME +"\":\"active\", \""+ Field.TYPE +"\":\"BOOLEAN\", \"" + Field.TEXT + "\":\"AKTIF\" }";

        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonNode = mapper.readValue(json, JsonNode.class);

        BooleanField field = (BooleanField) fieldResolverMapper.getResolver(jsonNode.get("type").asText()).resolve(jsonNode);

        Assert.assertEquals("active", field.getName());
        Assert.assertEquals("BOOLEAN", field.getType());
        Assert.assertEquals("AKTIF", field.getText());
    }
}
