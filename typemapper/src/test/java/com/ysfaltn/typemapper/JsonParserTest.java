package com.ysfaltn.typemapper;

import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.TypeModel;
import com.ysfaltn.typemapper.parser.JsonParser;
import junit.framework.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class JsonParserTest
{
    @Test
    public void shouldBeParseJson() throws IOException
    {
        String json = "[{\"type-name\":\"user\"}]";

        JsonParser jsonParser = new JsonParser();
        List<TypeModel> types = jsonParser.parse(json);

        Assert.assertEquals(1, types.size());
        Assert.assertEquals("user", types.get(0).getTypeName());
    }

    @Test
    public void shouldBeParseJsonAllValue() throws IOException
    {
        String json = "[{\"type-name\":\"user\", \"fields\":[" +
                                    "{\""+ Field.NAME +"\":\"active\", \""+ Field.TYPE +"\":\"BOOLEAN\", \"" + Field.TEXT + "\":\"AKTIF\" },"+
                                    "{\""+ Field.NAME +"\":\"birthDay\", \""+ Field.TYPE +"\":\"DATE\", \"" + Field.TEXT + "\":\"Dogum Tarihi\" },"+
                                    "{\""+ Field.NAME +"\":\"status\", \""+ Field.TYPE +"\":\"ENUM\", \"" + Field.TEXT + "\":\"Statusler\", \"values\":[{\"code\":\"active\", \"value\":\"Aktif\"}, {\"code\":\"passive\", \"value\":\"Pasif\"}]},"+
                                    "{\""+ Field.NAME +"\":\"id\", \""+ Field.TYPE +"\":\"INTEGER\", \"" + Field.TEXT + "\":\"Sira Numarasi\" },"+
                                    "{\""+ Field.NAME +"\":\"name\", \""+ Field.TYPE +"\":\"STRING\", \"" + Field.TEXT + "\":\"AD SOYAD\"},"+
                                    "{\""+ Field.NAME +"\":\"books\", \""+ Field.TYPE +"\":\"TYPE\", \"" + Field.TEXT + "\":\"Kitaplar\", \"ref\":{\"type\":\"book\", \"column\":\"id\"} }"+
                        "]}]";

        JsonParser jsonParser = new JsonParser();
        List<TypeModel> types = jsonParser.parse(json);

        Assert.assertEquals(1, types.size());
        Assert.assertEquals("user", types.get(0).getTypeName());
        Assert.assertEquals(6, types.get(0).getFields().size());
    }

    @Test
    public void shouldBeParseJsonAllValueWithSubType() throws IOException
    {
        String json = "[{\"type-name\":\"book\", \"fields\":[" +
                "{\""+ Field.NAME +"\":\"name\", \""+ Field.TYPE +"\":\"STRING\", \"" + Field.TEXT + "\":\"Name\" },"+
                "{\""+ Field.NAME +"\":\"id\", \""+ Field.TYPE +"\":\"INTEGER\", \"" + Field.TEXT + "\":\"Sira Numarasi\" }"+
                "]},{\"type-name\":\"user\", \"fields\":[" +
                "{\""+ Field.NAME +"\":\"active\", \""+ Field.TYPE +"\":\"BOOLEAN\", \"" + Field.TEXT + "\":\"AKTIF\" },"+
                "{\""+ Field.NAME +"\":\"birthDay\", \""+ Field.TYPE +"\":\"DATE\", \"" + Field.TEXT + "\":\"Dogum Tarihi\" },"+
                "{\""+ Field.NAME +"\":\"status\", \""+ Field.TYPE +"\":\"ENUM\", \"" + Field.TEXT + "\":\"Statusler\", \"values\":[{\"code\":\"active\", \"value\":\"Aktif\"}, {\"code\":\"passive\", \"value\":\"Pasif\"}]},"+
                "{\""+ Field.NAME +"\":\"id\", \""+ Field.TYPE +"\":\"INTEGER\", \"" + Field.TEXT + "\":\"Sira Numarasi\" },"+
                "{\""+ Field.NAME +"\":\"name\", \""+ Field.TYPE +"\":\"STRING\", \"" + Field.TEXT + "\":\"AD SOYAD\"},"+
                "{\""+ Field.NAME +"\":\"books\", \""+ Field.TYPE +"\":\"TYPE\", \"" + Field.TEXT + "\":\"Kitaplar\", \"ref\":{\"type\":\"book\", \"column\":\"id\"} }"+
                "]}]";;

        JsonParser jsonParser = new JsonParser();
        List<TypeModel> types = jsonParser.parse(json);

        Assert.assertEquals(2, types.size());
        Assert.assertEquals("user", types.get(0).getTypeName());
        Assert.assertEquals(6, types.get(0).getFields().size());
    }

}
