package com.ysfaltn.typemapper.codec;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class ClassConverter implements org.springframework.core.convert.converter.Converter<String, Class>
{
    @Override
    public Class convert(String s)
    {
        try
        {
            return Class.forName(s);
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        return null;
    }
}
