package com.ysfaltn.typemapper.codec;


import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class ClassCodec implements Codec<Class> {
    @Override
    public Class decode(BsonReader bsonReader, DecoderContext decoderContext) {
        return Class.class;
    }

    @Override
    public void encode(BsonWriter bsonWriter, Class aClass, EncoderContext encoderContext) {
        bsonWriter.writeString(aClass.toString());
    }

    @Override
    public Class<Class> getEncoderClass() {
        return Class.class;
    }
}
