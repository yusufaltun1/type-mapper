package com.ysfaltn.typemapper.adapter.checker;

import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.FieldValue;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public interface ValueChecker
{
    boolean check(FieldValue value, Field field);
}
