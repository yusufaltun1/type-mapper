package com.ysfaltn.typemapper.adapter.checker;

import com.ysfaltn.typemapper.model.EnumField;
import com.ysfaltn.typemapper.model.EnumValue;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.FieldValue;

import java.util.List;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class EnumValueChecker implements ValueChecker
{
    @Override
    public boolean check(FieldValue value, Field field) {
        if(field instanceof EnumField)
        {
            EnumField enumField = (EnumField) field;
            List<EnumValue> enumValues = enumField.getValues();
            for (EnumValue enumValue : enumValues)
            {
                String fieldValue = (String) value.getFieldValue();
                if(enumValue.getCode().equals(fieldValue))
                {
                    return true;
                }
            }
        }

        return false;
    }
}
