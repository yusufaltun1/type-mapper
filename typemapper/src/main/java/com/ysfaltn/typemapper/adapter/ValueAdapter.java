package com.ysfaltn.typemapper.adapter;

import com.ysfaltn.typemapper.adapter.checker.*;
import com.ysfaltn.typemapper.exception.FieldValueCastException;
import com.ysfaltn.typemapper.exception.FieldValueEmptyException;
import com.ysfaltn.typemapper.model.*;
import com.ysfaltn.typemapper.repository.TypeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;

/**
 * Created by yusufaltun on 08/11/2017.
 */
public class ValueAdapter
{
    private static final HashMap<Class, ValueChecker> valueCheckers = new HashMap<>();
    private static final HashMap<Class, ValueTypeChecker> typeCheckers = new HashMap<>();
    static {
        valueCheckers.put(EnumField.class, new EnumValueChecker());
        typeCheckers.put(EnumField.class, new EnumValueTypeChecker());
    }

    @Autowired
    private TypeRepository typeRepository;

    public void check(TypeValue typeValue)
    {
        TypeModel type = typeRepository.getType(typeValue.getTypeName());

        for (Field field : type.getFields())
        {
            FieldValue fieldValue = typeValue.getFieldValue(field);

            if (fieldValue != null)
            {
                ValueTypeChecker typeChecker = getTypeChecker(field);
                if (!typeChecker.checkType(field, fieldValue))
                {
                    throw new FieldValueCastException(type, field);
                }

                if (valueCheckers.containsKey(field.getClass()))
                {
                    ValueChecker checker = valueCheckers.get(field.getClass());
                    boolean correctly = checker.check(fieldValue, field);
                    if (!correctly)
                    {
                        throw new FieldValueNotCorrectlyException(type, field);
                    }
                }
            } else {
                if (field.isRequired()) {
                    throw new FieldValueEmptyException(type.getTypeName(), field.getName());
                }
            }
        }
    }

    public void setTypeRepository(TypeRepository typeRepository)
    {
        this.typeRepository = typeRepository;
    }

    public ValueTypeChecker getTypeChecker(Field field) {
        if(typeCheckers.containsKey(field.getClass()))
        {
            return typeCheckers.get(field.getClass());
        }
        else return new ValueTypeChecker();
    }
}
