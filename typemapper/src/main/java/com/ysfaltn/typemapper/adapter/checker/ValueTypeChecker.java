package com.ysfaltn.typemapper.adapter.checker;

import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.FieldValue;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class ValueTypeChecker
{
    public boolean checkType(Field field, FieldValue value)
    {
        if (field.getCls().isInstance(value.getFieldValue()))
        {
            return true;
        }
        else return false;
    }
}
