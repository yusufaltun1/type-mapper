package com.ysfaltn.typemapper.adapter.checker;

import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.FieldValue;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class EnumValueTypeChecker extends ValueTypeChecker
{
    public boolean checkType(Field field, FieldValue value)
    {
        if (String.class.isInstance(value.getFieldValue()))
        {
            return true;
        }
        else return false;
    }
}
