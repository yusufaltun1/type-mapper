package com.ysfaltn.typemapper.adapter.checker;

import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.TypeModel;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class FieldValueNotCorrectlyException extends RuntimeException
{
    public FieldValueNotCorrectlyException(TypeModel typeModel, Field field)
    {
        super("Field value is not correctly, type: "+ typeModel.getTypeName() +" fieldName: "+ field.getName());
    }
}
