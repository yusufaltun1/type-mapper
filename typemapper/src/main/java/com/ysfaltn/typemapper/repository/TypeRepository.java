package com.ysfaltn.typemapper.repository;

import com.ysfaltn.typemapper.exception.TypeNotFoundException;
import com.ysfaltn.typemapper.model.TypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TypeRepository
{
    @Autowired
    private MongoTemplate mongoTemplate;

    private static final List<TypeModel> types = new ArrayList<>();
    public void save(TypeModel type)
    {
        mongoTemplate.save(type);
    }

    public List<TypeModel> get()
    {
        List<TypeModel> list = mongoTemplate.findAll(TypeModel.class).stream().collect(Collectors.toList());
        types.clear();
        types.addAll(list);

        return list;
    }

    public TypeModel getType(String typeName)
    {
        List<TypeModel> types = get();
        for (TypeModel type : types) {
            if(type.getTypeName().equals(typeName))
            {
                return type;
            }
        }

        throw new TypeNotFoundException(typeName);
    }
}