package com.ysfaltn.typemapper.repository;

import com.ysfaltn.typemapper.adapter.ValueAdapter;
import com.ysfaltn.typemapper.model.TypeValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by yusufaltun on 14/07/2017.
 */
public class FieldRepository
{
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ValueAdapter valueAdapter;

    public void save(TypeValue value)
    {
        valueAdapter.check(value);
        mongoTemplate.save(value);
    }

}
