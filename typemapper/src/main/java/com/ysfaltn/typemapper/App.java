package com.ysfaltn.typemapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.lcwaikiki.sap.ecommerce.ext.ekol.model.Order;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.FieldValue;
import com.ysfaltn.typemapper.model.TypeModel;
import com.ysfaltn.typemapper.model.TypeValue;
import com.ysfaltn.typemapper.model.builder.TypeValueBuilder;
import com.ysfaltn.typemapper.parser.JsonParser;
import com.ysfaltn.typemapper.repository.FieldRepository;
import com.ysfaltn.typemapper.repository.TypeRepository;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args ) throws IOException
    {

        Long l = 1l;
        Long l1 = 1l;

        if(l == l1)
        {
            System.out.println(l + l1);
        }
//        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
//// or SimpleDateFormat sdf = new SimpleDateFormat( "MM/dd/yyyy KK:mm:ss a Z" );
//        sdf.setTimeZone( TimeZone.getDefault() );
//        System.out.println( sdf.format( new Date() ) );
//        System.out.println(TimeZone.getDefault().getOffset(new Date().getTime()) / 1000 / 60 );

        String json = "[{\"ekolCode\":\"171017113644684576000VQGXJR\",\"orderNumber\":\"cons_1712191959500\",\"status\":\"PreparedOrder\",\"products\":[{\"name\":null,\"barcode\":\"8681362185578\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681363377262\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681359377535\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681361616820\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681360107527\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681360642257\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681363371765\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681363233674\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681362815956\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681362078009\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681361000827\",\"price\":0.0,\"unitCode\":null,\"quantity\":1,\"reason\":null,\"outPlanned\":0}],\"packages\":[{\"barcode\":\"5233192023\",\"volumetricWeight\":8.5,\"weight\":1.0,\"products\":[{\"name\":null,\"barcode\":\"8681362185578\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681363377262\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681359377535\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681361616820\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681360107527\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681360642257\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681363371765\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681363233674\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681362815956\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681362078009\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0},{\"name\":null,\"barcode\":\"8681361000827\",\"price\":0.0,\"unitCode\":\"PIE\",\"quantity\":1,\"reason\":null,\"outPlanned\":0}]}]}]";
        ObjectMapper objectMapper = new ObjectMapper();
        List<Order> orders = objectMapper.readValue(json, new TypeReference<List<Order>>() { });
        System.out.println(orders);
    }
    private static BigDecimal scale(BigDecimal d)
    {
        return d.setScale(2, BigDecimal.ROUND_HALF_DOWN);
    }

}
