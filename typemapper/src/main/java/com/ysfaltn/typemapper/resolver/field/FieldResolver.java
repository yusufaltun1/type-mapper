package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.Field;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public interface FieldResolver
{
    Field resolve(JsonNode node);
}
