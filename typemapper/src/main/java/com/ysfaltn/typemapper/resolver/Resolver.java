package com.ysfaltn.typemapper.resolver;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public interface Resolver
{
    void resolve();
}
