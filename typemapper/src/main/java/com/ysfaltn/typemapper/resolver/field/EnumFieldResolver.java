package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.EnumField;
import com.ysfaltn.typemapper.model.EnumValue;
import com.ysfaltn.typemapper.model.Field;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class EnumFieldResolver extends AbstractFieldResolver implements FieldResolver
{
    @Override
    public Field resolve(JsonNode node)
    {
        EnumField field = new EnumField();
        super.resolve(field, node);
        JsonNode valuesNode = node.get(EnumField.VALUES);
        List<EnumValue> enumValues = new ArrayList<>();
        if (valuesNode != null)
        {
            if(valuesNode.isArray())
            {
                 for (JsonNode value : valuesNode)
                 {
                     EnumValue enumValue = new EnumValue();
                     enumValue.setCode(value.get(EnumField.VALUE_CODE).asText());
                     enumValue.setValue(value.get(EnumField.VALUE_VALUE).asText());
                     enumValues.add(enumValue);
                 }
            }
            else
            {
                throw new RuntimeException("Values field must be array for field "+ field.getName());
            }
        }

        field.setValues(enumValues);
        return field;
    }
}
