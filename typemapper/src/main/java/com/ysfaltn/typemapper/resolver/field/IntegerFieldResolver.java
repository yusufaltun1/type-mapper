package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.IntegerField;
import com.ysfaltn.typemapper.model.StringField;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class IntegerFieldResolver extends AbstractFieldResolver implements FieldResolver {

    public Field resolve(JsonNode node) {
        IntegerField field = new IntegerField();
        super.resolve(field, node);
        return field;
    }
}
