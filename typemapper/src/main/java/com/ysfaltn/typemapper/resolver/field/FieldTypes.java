package com.ysfaltn.typemapper.resolver.field;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public enum FieldTypes
{
    INTEGER,
    DATE,
    LIST,
    STRING,
    BOOLEAN,
    TYPE, ENUM;
}
