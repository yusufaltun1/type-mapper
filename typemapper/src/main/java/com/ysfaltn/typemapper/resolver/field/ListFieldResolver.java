package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.IntegerField;
import com.ysfaltn.typemapper.model.ListField;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class ListFieldResolver extends AbstractFieldResolver implements FieldResolver {
    public Field resolve(JsonNode node) {
        ListField field = new ListField();
        super.resolve(field, node);
        return field;
    }
}
