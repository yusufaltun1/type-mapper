package com.ysfaltn.typemapper.resolver.field;

import java.util.HashMap;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class FieldResolverMapper
{
    private static final HashMap<FieldTypes, FieldResolver> resolvers = new HashMap<>();
    static
    {
        resolvers.put(FieldTypes.BOOLEAN, new BooleanFieldResolver());
        resolvers.put(FieldTypes.DATE, new DateFieldResolver());
        resolvers.put(FieldTypes.INTEGER, new IntegerFieldResolver());
        resolvers.put(FieldTypes.LIST, new ListFieldResolver());
        resolvers.put(FieldTypes.STRING, new StringFieldResolver());
        resolvers.put(FieldTypes.TYPE, new TypeFieldResolver());
        resolvers.put(FieldTypes.ENUM, new EnumFieldResolver());
    }

    public FieldResolver getResolver(String type)
    {
        FieldTypes fieldType;
        try
        {
            fieldType = FieldTypes.valueOf(type);
        }
        catch (IllegalArgumentException e)
        {
            throw new RuntimeException("Field not supported "+ type );
        }


        if(resolvers.containsKey(fieldType))
        {
            return resolvers.get(fieldType);
        }

        return null;
    }
}
