package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.Field;

/**
 * Created by yusufaltun on 08/11/2017.
 */
public class AbstractFieldResolver
{
    protected void resolve(Field field, JsonNode node)
    {
        field.setName(node.get(Field.NAME).asText());
        field.setType(node.get(Field.TYPE).asText());
        field.setText(node.get(Field.TEXT).asText());
    }
}
