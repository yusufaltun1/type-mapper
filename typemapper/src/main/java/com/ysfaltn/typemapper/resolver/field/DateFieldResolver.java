package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.DateField;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.IntegerField;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class DateFieldResolver extends AbstractFieldResolver implements FieldResolver {
    public Field resolve(JsonNode node)
    {
        DateField field = new DateField();
        super.resolve(field, node);
        return field;
    }
}
