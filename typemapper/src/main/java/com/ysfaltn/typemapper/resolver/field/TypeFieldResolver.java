package com.ysfaltn.typemapper.resolver.field;

import com.fasterxml.jackson.databind.JsonNode;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.Reference;
import com.ysfaltn.typemapper.model.StringField;
import com.ysfaltn.typemapper.model.TypeField;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class TypeFieldResolver extends AbstractFieldResolver implements FieldResolver {
    public Field resolve(JsonNode node)
    {
        TypeField field = new TypeField();
        super.resolve(field, node);

        JsonNode refJson = node.get(TypeField.REF);
        Reference reference = new Reference();
        reference.setColumn(refJson.get(Reference.COLUMN).asText());
        reference.setType(refJson.get(Reference.TYPE).asText());

        field.setReference(reference);
        return field;
    }
}
