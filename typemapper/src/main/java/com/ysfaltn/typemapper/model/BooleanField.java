package com.ysfaltn.typemapper.model;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class BooleanField extends Field {

    public BooleanField() {
        super(Boolean.class);
    }
}
