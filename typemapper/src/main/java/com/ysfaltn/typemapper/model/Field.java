package com.ysfaltn.typemapper.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Transient;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public abstract class Field
{
    public static final String NAME = "field-name";
    public static final String TYPE = "type";
    public static final String TEXT = "text";
    public static final String REQUIRED = "required";

    private String _id;
    private String type;
    private String name;
    private String text;
    private boolean required = false;

    private String className;

    public Field(Class cls)
    {
        this.className = cls.getName();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public Class getCls()
    {
        try {
            return Class.forName(className);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }
}
