package com.ysfaltn.typemapper.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class TypeModel
{
    public static final String FIELDS = "fields";
    private String typeName;
    private List<Field> fields;
    private String _id;
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<Field> getFields() {
        if(fields == null)
        {
            fields = new ArrayList<>();
        }
        return fields;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
