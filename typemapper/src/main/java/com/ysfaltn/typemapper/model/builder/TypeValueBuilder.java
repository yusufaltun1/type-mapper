package com.ysfaltn.typemapper.model.builder;

import com.ysfaltn.typemapper.model.FieldValue;
import com.ysfaltn.typemapper.model.TypeValue;

import java.util.HashMap;

/**
 * Created by yusufaltun on 07/11/2017.
 */
public class TypeValueBuilder
{
    private String typeName;
    private HashMap<String, Object> values = new HashMap<>();
    private TypeValue typeValue = null;

    private TypeValueBuilder(String typeName)
    {
        typeValue = new TypeValue();
        typeValue.setTypeName(typeName);
    }

    public static TypeValueBuilder create(String typeName)
    {
        return new TypeValueBuilder(typeName);
    }

    public TypeValueBuilder addValue(FieldValue value)
    {
        typeValue.addValue(value);
        return this;
    }

    public TypeValue done()
    {
        return typeValue;
    }
}
