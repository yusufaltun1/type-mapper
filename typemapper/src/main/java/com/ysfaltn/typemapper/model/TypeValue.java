package com.ysfaltn.typemapper.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.HashMap;

/**
 * Created by yusufaltun on 07/11/2017.
 */
public class TypeValue
{
    private HashMap<String, FieldValue> values = new HashMap<>();
    private String typeName;

    @JsonIgnore
    public TypeValue addValue(FieldValue value)
    {
        values.put(value.getFieldName(), value);
        return this;
    }

    public void setValues(HashMap<String, FieldValue> values) {
        this.values = values;
    }

    public String getTypeName()
    {
        return typeName;
    }

    public void setTypeName(String typeName)
    {
        this.typeName = typeName;
    }

    public HashMap<String, FieldValue> getValues() {
        return values;
    }

    @JsonIgnore
    public FieldValue getFieldValue(Field field)
    {
        if(values.containsKey(field.getName()))
        {
            return values.get(field.getName());
        }

        return null;
    }
}
