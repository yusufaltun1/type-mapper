package com.ysfaltn.typemapper.model;

import java.util.Date;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class DateField extends Field {
    public DateField() {
        super(Date.class);
    }
}
