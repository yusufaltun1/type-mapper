package com.ysfaltn.typemapper.model;

/**
 * Created by yusufaltun on 09/11/2017.
 */
public class EnumValue
{
    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
