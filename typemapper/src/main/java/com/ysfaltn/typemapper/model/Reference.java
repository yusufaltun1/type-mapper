package com.ysfaltn.typemapper.model;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class Reference
{
    public static final String TYPE = "type";
    public static final String COLUMN = "column";
    private String typeName;
    private String column;

    public String getType() {
        return typeName;
    }

    public void setType(String type) {
        this.typeName = type;
    }

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }
}
