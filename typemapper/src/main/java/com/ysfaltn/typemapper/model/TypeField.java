package com.ysfaltn.typemapper.model;


/**
 * Created by yusufaltun on 10/07/2017.
 */
public class TypeField extends Field
{
    public static final String REF = "ref";
    private Reference reference;

    public TypeField() {
        super(Object.class);
    }

    public void setReference(Reference reference) {
        this.reference = reference;
    }

    public Reference getReference() {
        return reference;
    }
}
