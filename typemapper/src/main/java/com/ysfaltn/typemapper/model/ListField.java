package com.ysfaltn.typemapper.model;

import java.util.List;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class ListField extends Field {

    public ListField() {
        super(List.class);
    }
}
