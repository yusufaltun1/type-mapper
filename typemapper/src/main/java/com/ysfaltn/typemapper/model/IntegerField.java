package com.ysfaltn.typemapper.model;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class IntegerField extends Field
{

    public IntegerField() {
        super(Integer.class);
    }
}
