package com.ysfaltn.typemapper.model;


/**
 * Created by yusufaltun on 10/07/2017.
 */
public class StringField extends Field {
    public StringField() {
        super(String.class);
    }
}
