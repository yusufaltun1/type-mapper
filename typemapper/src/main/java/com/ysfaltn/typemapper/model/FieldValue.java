package com.ysfaltn.typemapper.model;

/**
 * Created by yusufaltun on 07/11/2017.
 */
public class FieldValue
{
    private final String fieldName;
    private final Object fieldValue;

    public FieldValue(String fieldName, Object fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Object getFieldValue() {
        return fieldValue;
    }

}
