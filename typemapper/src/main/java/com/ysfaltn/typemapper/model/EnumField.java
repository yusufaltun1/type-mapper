package com.ysfaltn.typemapper.model;

import java.util.List;

/**
 * Created by yusufaltun on 11/07/2017.
 */
public class EnumField extends Field
{
    public static final String VALUES = "values";
    public static final String VALUE_CODE = "code";
    public static final String VALUE_VALUE = "value";
    private List<EnumValue> values;

    public EnumField() {
        super(Enum.class);
    }

    public List<EnumValue> getValues() {
        return values;
    }

    public void setValues(List<EnumValue> values) {
        this.values = values;
    }
}
