package com.ysfaltn.typemapper.exception;

import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.TypeModel;

/**
 * Created by yusufaltun on 08/11/2017.
 */
public class FieldValueCastException extends RuntimeException
{
    public FieldValueCastException(TypeModel typeModel, Field field)
    {
        super("FieldValue type must be "+ field.getCls().getName() +" field : "+ field.getName() +" on Type: "+ typeModel.getTypeName());
    }
}
