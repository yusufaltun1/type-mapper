package com.ysfaltn.typemapper.exception;


/**
 * Created by yusufaltun on 08/11/2017.
 */
public class TypeNotFoundException extends RuntimeException
{
    public TypeNotFoundException(String typeName)
    {
        super("Type not found, TypeName: "+ typeName);
    }
}
