package com.ysfaltn.typemapper.exception;

/**
 * Created by yusufaltun on 08/11/2017.
 */
public class FieldValueEmptyException extends RuntimeException
{
    public FieldValueEmptyException(String typeName, String fieldName)
    {
        super("Field must not be empty : "+ typeName +"."+ fieldName);
    }
}
