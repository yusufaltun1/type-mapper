package com.ysfaltn.typemapper.parser;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ysfaltn.typemapper.model.Field;
import com.ysfaltn.typemapper.model.TypeModel;
import com.ysfaltn.typemapper.resolver.field.FieldResolver;
import com.ysfaltn.typemapper.resolver.field.FieldResolverMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 10/07/2017.
 */
public class JsonParser
{
    public List<TypeModel> parse(String jsonString) throws IOException
    {
        FieldResolverMapper fieldResolverMapper = new FieldResolverMapper();
        List<TypeModel> types = new ArrayList<TypeModel>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode object = mapper.readValue(jsonString, JsonNode.class);

        if (object.isArray())
        {
            for (JsonNode typeNode : object)
            {
                TypeModel type = new TypeModel();
                type.setTypeName(typeNode.get("type-name").asText());
                JsonNode fieldNodes = typeNode.get(TypeModel.FIELDS);

                if (fieldNodes != null)
                {
                    if (fieldNodes.isArray())
                    {
                        for (JsonNode fieldNode : fieldNodes)
                        {
                            Field field = fieldResolverMapper.getResolver(fieldNode.get("type").asText()).resolve(fieldNode);
                            type.getFields().add(field);
                        }
                    }
                    else {
                        throw new RuntimeException("Fields attribute must be array for type "+ type.getTypeName());
                    }
                }
                types.add(type);
            }
        }

        return types;
    }
}
