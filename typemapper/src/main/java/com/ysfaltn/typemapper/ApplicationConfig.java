package com.ysfaltn.typemapper;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.ysfaltn.typemapper.adapter.ValueAdapter;
import com.ysfaltn.typemapper.codec.ClassCodec;
import com.ysfaltn.typemapper.codec.ClassConverter;
import com.ysfaltn.typemapper.parser.JsonParser;
import com.ysfaltn.typemapper.repository.FieldRepository;
import com.ysfaltn.typemapper.repository.TypeRepository;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class ApplicationConfig
{
    @Bean
    public MongoClient mongo() throws Exception {
        CodecRegistry codecRegistry =
                CodecRegistries.fromRegistries(
                        CodecRegistries.fromCodecs(new ClassCodec()),
                        MongoClient.getDefaultCodecRegistry());
        MongoClientOptions options = MongoClientOptions.builder()
                .codecRegistry(codecRegistry).build();

        GenericConversionService conversionService = (GenericConversionService) DefaultConversionService.getSharedInstance();
        conversionService.addConverter(new ClassConverter());
        return new MongoClient("localhost",options);
    }
 
    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), "type_mapper");
    }

    @Bean
    public TypeRepository typeRepository()
    {
        return new TypeRepository();
    }

    @Bean
    public FieldRepository fieldRepository()
    {
        return new FieldRepository();
    }

    @Bean
    public JsonParser parser()
    {
        return new JsonParser();
    }

    @Bean
    public ValueAdapter valueAdapter()
    {
        return new ValueAdapter();
    }
}
